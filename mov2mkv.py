import os
import subprocess

target_size = 1 * 1024 * 1024 * 1024  # 1 GB

input_dir = "/run/media/leny/9C33-6BBD/DCIM/100CANON/"
output_dir = "/home/leny/videos_from_camera"
count = 0

for filename in os.listdir(input_dir):
    if filename.endswith(".MOV"):
        count = count + 1

        input_file = os.path.join(input_dir, filename)
        file_size = os.path.getsize(input_file)
        output_file = os.path.join(output_dir, os.path.splitext(filename)[0] + ".mkv")

        subprocess.run(
            [
                "ffmpeg",
                "-i",
                input_file,
                "-c:v",
                "libx264",
                "-crf",
                "28",
                "-c:a",
                "aac",
                "-b:a",
                "192k",
                output_file,
            ]
        )

        print(f"Converted {filename} to {os.path.basename(output_file)}")
        print(f"Original file size: {file_size / (1024 * 1024 * 1024):.2f} GB")
        # print(
        #     f"Compressed file size: {os.path.getsize(output_file) / (1024 * 1024 * 1024):.2f} GB"
        # )

print(f"moved {count} files")
