import lib
import argparse
from pathlib import Path
from interactive import PushdownInteractive


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "minutes", help="how many minutes until next pushups", type=float)
    parser.add_argument("goal", help="how many pushups to do", type=int)
    parser.add_argument("log_dir", help="the", type=Path)
    args = parser.parse_args()

    PushdownInteractive(args).cmdloop()
