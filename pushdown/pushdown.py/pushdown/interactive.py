import humanize
import cmd
import lib
import logs
from dateutil.parser import parse


def make_summary(log: dict, goal: int):
    """takes a log dict and makes it suitable for notification"""
    d = humanize.naturaltime(parse(log["Datetime"]))
    message = f"you last did: <b>{log['Count']}</b> pushups <i>{d}</i>."
    message += f"\nGoal = {goal}"
    return message


class PushdownInteractive(cmd.Cmd):
    """Pushdown interactive"""

    intro = ("Pushdown! let's go!.\n"
             "Ctrl+C to quit")
    prompt = "❯❯ "

    def __init__(self, args):
        self.seconds = int(60 * args.minutes)
        self.goal = args.goal
        self.log_dir = args.log_dir
        self.previous_count = 0
        super(PushdownInteractive, self).__init__()
        self.do_repeat(f"{args.minutes}")

    def do_once(self, line):
        """Start the countdown for one push up"""
        args = [a for a in line.split()]
        if args:
            self.seconds = int(60 * float(args[0]))

        lib.countdown(self.seconds)
        summary = make_summary(logs.get_last(self.log_dir), self.goal)
        lib.notify("It is time for pushups!", summary)

        actual = input(f"How many did you do?\nGoal = {self.goal}: ")
        if not actual:
            actual = str(self.goal)

        logs.log(self.log_dir, int(actual.strip()))

    def do_repeat(self, line):
        """Start the countdown and keep starting it repeatedly"""
        self.do_once(line)
        actual = input("continue? ")
        # instead of a prompt, add an interrupt. Ctrl+C or Ctrl+D or Ctrl+Q
        response = actual.lower().strip()
        if response in ["yes", "y", "yah", "ofcourse", "you know it"]:
            return self.do_repeat(line)
        return True
