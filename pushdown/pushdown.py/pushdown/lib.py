import subprocess
import sys
import time


def countdown(t: int):
    """counts down till then next pushup"""
    while t:
        mins = t // 60
        secs = t % 60
        # TODO: use a function here.
        sys.stdout.write(f"\033[K{mins:02d}:{secs:02d}\r")
        time.sleep(1)
        t -= 1
    sys.stdout.write("\n")


def notify(summary: str, message: str):
    """Uses subprocess to call notify-send"""
    subprocess.call(["notify-send",
                     "--app-name=Pushdown",
                     "--urgency=critical",
                     "--icon=ktimer",
                     f"{summary}",
                     f"{message}"
                     ])
