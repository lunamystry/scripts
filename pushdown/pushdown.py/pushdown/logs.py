from datetime import datetime, date
from pathlib import Path
import os
import csv


def get_filename(log_dir: Path):
    """make sure the log directory exists and then make"""
    if not log_dir.exists():
        log_dir.mkdir(parents=True, exist_ok=True)
    return log_dir / f"{date.today()}.csv.log"


def log(log_dir: Path, count: int, dt: datetime = None, comment: str = "<no comment>"):
    """Logs the number of push ups done at dt. If dt is None, then the current
    time is used."""
    if dt is None:
        dt = datetime.now()

    fieldnames = ["Datetime", "Count", "Comment"]
    pushup_log = get_filename(log_dir)
    if not pushup_log.exists():
        with pushup_log.open("a+") as file:
            writer = csv.DictWriter(file, fieldnames=fieldnames)
            writer.writeheader()

    with pushup_log.open("a+") as file:
        writer = csv.DictWriter(file, fieldnames=fieldnames)
        writer.writerow({"Datetime": dt, "Count": count, "Comment": comment})


def get_last(log_dir: Path):
    """Get's the last done pushup"""
    log_files = os.listdir(log_dir)
    sorted_dates = sorted([d.replace(".csv.log", "") for d in log_files])
    latest_log = log_dir / f"{sorted_dates[-1]}.csv.log"
    with latest_log.open(newline='') as csvfile:
        reader = csv.DictReader(csvfile)
        for row in reader:
            # what is this efficiency of which you speak? The log files are small.
            pass
        return row
