# Pushdown

Keeping track of push-ups I guess

## Features

- I want a reminder to do push-ups
- I want to do push-ups based on my goals
- Goal are like:
  - 20 push-ups every 60min
  - x push-ups where x increases normally till y then decrease till x repeat all
    this 5 times
  - basically a Goal is a template of the push-ups I want to do
- Log how many push-ups I managed to do
  - Allow me to add a comment on how it was if I want
