val scala3Version = "3.4.2"

lazy val main = project
  .in(file("."))
  .enablePlugins(NativeImagePlugin)
  .settings(
    name := "pushdown.scala",
    version := "1.0.0",
    scalaVersion := scala3Version,
    organization := "me.mandla",
    Compile / mainClass := Some("me.mandla.pushdown.Main"),
    nativeImageOptions ++= List(
      "--allow-incomplete-classpath",
      "--report-unsupported-elements-at-runtime",
      "--initialize-at-build-time",
      "--no-fallback"
    ),
    libraryDependencies ++= Seq(
      "dev.zio" %% "zio" % "2.0.19",
      "dev.zio" %% "zio-cli" % "0.5.0",
      "org.scalameta" %% "munit" % "1.0.0" % Test
    )
  )
