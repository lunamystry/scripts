package me.mandla
package pushdown.cli

import zio.Console.printLine
import zio.cli.HelpDoc.Span.text
import zio.cli._

import java.nio.file.{Path => JPath}

// Example from: https://zio.dev/zio-cli/
object CLI extends ZIOCliDefault {
  import java.nio.file.Path

  // All the possible subcommands
  sealed trait Subcommand extends Product with Serializable
  object Subcommand {
    final case class Add(modified: Boolean, directory: JPath) extends Subcommand
    final case class Remote(verbose: Boolean) extends Subcommand
    object Remote {
      sealed trait RemoteSubcommand extends Subcommand
      final case class Add(name: String, url: String) extends RemoteSubcommand
      final case class Remove(name: String) extends RemoteSubcommand
    }
  }

  // Common options
  val modifiedFlag: Options[Boolean] = Options.boolean("m")
  val verboseFlag: Options[Boolean] = Options.boolean("verbose").alias("v")
  val configPath: Options[Path] = Options.directory("c", Exists.Yes)

  // `git add` command
  val add: Command[Subcommand] = {
    val help = HelpDoc.p("Add subcommand description")
    val directoryArg = Args.directory("directory")
    Command("add", modifiedFlag, directoryArg)
      .withHelp(help)
      // This converts the tuple of options and args into a Subcommand
      .map { case (modified, directory) =>
        Subcommand.Add(modified, directory)
      }
  }

  // `git remote add` command
  val remoteAdd: Command[Subcommand] = {
    val help: HelpDoc = HelpDoc.p("Add remote subcommand description")
    val nameFlag = Options.text("name")
    val urlFlag = Options.text("url")
    Command("add", nameFlag ++ urlFlag)
      .withHelp(help)
      .map { case (name, url) =>
        Subcommand.Remote.Add(name, url)
      }
  }

  // `git remote remove` command
  val remoteRemove: Command[Subcommand] = {
    val help: HelpDoc = HelpDoc.p("Remove remote subcommand description")
    val nameArg = Args.text("name")
    Command("remove", nameArg)
      .withHelp(help)
      .map(
        Subcommand.Remote.Remove(_)
      ) // could also do Subcommand.Remote.Remove.apply
  }

  // `git remote <subcommand>` command
  // combines the removeAdd and removeRemove from above
  val remote: Command[Subcommand] = {
    val help: HelpDoc = HelpDoc.p("Remote subcommand description")
    Command("remote", verboseFlag)
      .withHelp(help)
      .map(Subcommand.Remote(_))
      .subcommands(remoteAdd, remoteRemove)
      .map(_._2) // Don't know why the first item of the tuple is ignored here.
  }

  // `git` command
  // combines the add and remote commands from above
  val git: Command[Subcommand] =
    Command("git", Options.none, Args.none).subcommands(add, remote)

  // Create the CLI app
  val cliApp = CliApp.make(
    name = "Git Version Control",
    version = "0.9.2",
    summary = text("a client for the git dvcs protocol"),
    command = git
  ) {
    case Subcommand.Add(modified, directory) =>
      printLine(
        s"Executing `git add $directory` with modified flag set to $modified"
      )
    case Subcommand.Remote.Add(name, url) =>
      printLine(s"Executing `git remote add $name $url`")
    case Subcommand.Remote.Remove(name) =>
      printLine(s"Executing `git remote remove $name`")
    case Subcommand.Remote(verbose) =>
      printLine(s"Executing `git remote` with verbose flag set to $verbose")
  }
}
