package me.mandla
package pushdown

import pushdown.cli.CLI
import zio.cli.ZIOCliDefault

object Main extends ZIOCliDefault {
  val cliApp = CLI.cliApp
}
