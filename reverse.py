#!/usr/bin/env python3

import os
import click
from typing import List


@click.command()
@click.argument(
    "base_dir", type=click.Path(exists=True, file_okay=False, writable=True)
)
@click.option(
    "--dry-run",
    is_flag=True,
    default=False,
    help="Simulate the backup process without actually performing it.",
)
@click.argument("directories", type=click.Path(exists=True, file_okay=False), nargs=-1)
def backup_directories(base_dir: str, dry_run: bool, directories: List[str]) -> None:
    """
    Backs up the given directories to the specified base directory using rsync.
    """
    for directory in directories:
        backup_dir = os.path.join(base_dir, os.path.basename(directory))
        if dry_run:
            click.echo(f"Would create directory: {backup_dir}")
        else:
            os.makedirs(backup_dir, exist_ok=True)

    rsync_command = [
        "rsync",
        "-rlptgoDAX",
    ]

    for directory in directories:
        backup_dir = os.path.join(base_dir, os.path.basename(directory))
        if dry_run:
            click.echo(
                f"Would run: {' '.join(rsync_command)} '{directory}/' '{backup_dir}/'"
            )
        else:
            os.system(f"{' '.join(rsync_command)} '{directory}/' '{backup_dir}/'")

    if dry_run:
        click.echo("Dry run complete.")
    else:
        click.echo("Backup complete.")


if __name__ == "__main__":
    backup_directories()
