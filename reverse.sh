#!/usr/bin/env bash

# --archive
#   -r: This flag enables recursive copying, which means it will copy directories and their contents.
#   -l: This flag preserves symbolic links.
#   -p: This flag preserves file permissions.
#   -t: This flag preserves file modification times.
#   -g: This flag preserves group ownership.
#   -o: This flag preserves owner (user) information.
#   -D: This flag preserves device files (character and block) and special files.
#
#   -A: This flag preserves ACLs (Access Control Lists) on the files and directories being copied.
#   -X: This flag preserves extended attributes on the files and directories being copied.
#   -U: This flag preserves the user and group ownership of the files and directories being copied, even if the ownership is different on the source and destination.
#   -N: This flag preserves the nanosecond resolution of file modification times.
#   -H: This flag preserves hard links between files.

# Check if the required arguments are provided
if [ "$#" -lt 2 ]; then
  echo "Usage: $0 <base_dir> [--dry-run] <dir1> [dir2] [dir3] ..."
  exit 1
fi

# Set the base directory for the backups
BASE_DIR="$1"
shift

# Check if the --dry-run flag is present
if [ "$1" == "--dry-run" ]; then
  DRY_RUN=true
  shift
else
  DRY_RUN=false
fi

# Set the list of directories to back up
DIRS=("$@")

# Create the backup directory structure
for dir in "${DIRS[@]}"; do
  backup_dir="$BASE_DIR/$(basename "$dir")"
  if [ "$DRY_RUN" = true ]; then
    echo "Would create directory: $backup_dir"
  else
    mkdir -p "$backup_dir"
  fi
done

# Perform the backup using rsync
for dir in "${DIRS[@]}"; do
  backup_dir="$BASE_DIR/$(basename "$dir")"
  if [ "$DRY_RUN" = true ]; then
    echo "Would run: rsync <--flags> \"$dir/\" \"$backup_dir/\""
  else
    rsync \
      --human-readable \
      --progress \
      --archive \
      --acls \
      --xattrs \
      "$dir/" "$backup_dir/"
  fi
done

if [ "$DRY_RUN" = true ]; then
  echo "Dry run complete."
else
  echo "Backup complete."
fi
